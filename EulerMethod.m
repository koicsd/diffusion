function data_fin = EulerMethod(Rinit, Rfin, data_init, Tinit, Tfin, Nt, kerfun, varargin)
%data_fin = EulerMethod(Rinit, Rfin, data_init, Tinit, Tfin, Nt, kerfun, varargin)
%
%   An Euler's method based solver
%   for partial differential equations.
%   Calculates values at time-point Tfin
%   from initial values at time-point Tinit,
%   using Nt time-steps.
%
%   Parameters:
%       Rinit:      a vector containing the lower limits of spacious axes.
%                   numel(Rinit) has to be equal to
%                   the number of spacius dimensions;
%                   in case of a 1-dimensional problem
%                   Rinit must be a scalar.
%
%       Rfin:       a vector containing the upper limits of spacious axes.
%                   numel(Rfin) has to be equal to
%                   the number of spacius dimensions;
%                   in case of a 1-dimensional problem
%                   Rfin must be a scalar
%
%       data_init:  the initial data given at time-point Tinit.
%                   in case of a 1-dimensional problem it must be a vector;
%                   in case of a 2-dimensional problem it must be a matrix;
%                   in future versions, in case of a 3-dimensional problem
%                   it must be a 3D array
%
%       Tinit:      the initial time-point, data_init belongs to
%
%       Tfin:       the time-point when values (data_fin)
%                   should be calculated
%
%       Nt:         the number of time-steps or iterations,
%                   each time-step is a separate iteration
%
%       kerfun:     a function handle containing
%                   the problem and boundary condition,
%                   with syntax:
%                   [kernel, precond] = kerfun(param, Rinit, Rfin, Nr)
%                   where:
%                       kernel:     a sparse-matrix used to calculate
%                                   derivatives in each iteration.
%                                   size must be:
%                                   numel(data_init) by numel(data_init)
%                       precond:    a sparse-matrix used to precondition
%                                   data before first iteration
%                                   size must be:
%                                   numel(data_init) by numel(data_init)
%                       Nr:         the number of spacious data-points
%                                   in similar format as Rinit and Rfin
%                                   (see above)
%
%       varargin:   the parameters to be given to kerfun
%
%   Return value:
%       data_fin:   the datapoints calculated at time-point Tfin,
%                   with the same size as data_init

if ~isscalar(Tinit) || ~isscalar(Tfin) || ~isscalar(Nt)
    error('Tinit, Tfin and Nt must be a scalar!')
end

dt=(Tfin-Tinit)/Nt;  %% time step
Nr = size(data_init);  %% number of datapoints along different axes as vector
Nflat = prod(Nr);  % numel(Nr)  %% total number of datapoints

if isvector(data_init)
    if ~isscalar(Rinit) || ~isscalar(Rfin)
        error('Rinit and Rfin must be a scalar if data_init is a single vector!')
    end
    [kernel, precond] = kerfun(Rinit, Rfin, Nflat, varargin{:});  %% creating sparse-matrices
else
    [kernel, precond] = kerfun(Rinit, Rfin, Nr, varargin{:});  %% creating sparse-matrices
end

flatdata = precond * reshape(data_init, Nflat, 1);  %% flattening and preprocessing
for i = 1 : Nt  %% processing flattened data (iteration over time)
    flatdata = flatdata + dt * kernel * flatdata;
end
% flatdata = (speye(Nflat) + kernel * dt) ^ Nt * flatdata;  %% processing flattened data, seems to be slow
data_fin = reshape(flatdata, Nr);
end

