% A script to test EulerMethod for 1D Diffusion

clear;

% Time and space domain:
Xinit=0; Xfin=1; Nx=50; dx=(Xfin-Xinit)/(Nx-1);
Tinit=0; Tfin=0.01; Nt=10000; dt=(Tfin-Tinit)/Nt;

% Test-function 1: Sine wave
% A=1;
% k=1.2;
% phi=0;
% X=Xinit:dx:Xfin;
% Uinit=A*cos(2*pi*k*X-phi);

% Test-function 2: Gaussian
A=1;
sigm=0.3;
mu = 0.4;
phi=pi/4;
X=Xinit:dx:Xfin;
Uinit=(A/sigm/sqrt(2*pi))*exp(-((X-mu)/sigm).^2/2);

% Plot and pause:
plot(X, Uinit)
pause

% Solve and display:
D=1;  % Diffusion coefficient
disp('Fixed boundary:')
tic; Ufixbc=EulerMethod(Xinit, Xfin, Uinit, Tinit, Tfin, Nt, @KerMat_Diffusion_FixedBC, D); toc
disp('Periodic:')
tic; Uperiod=EulerMethod(Xinit, Xfin, Uinit, Tinit, Tfin, Nt, @KerMat_Diffusion_Periodic, D); toc
disp('Isolated:')
tic; Uisolated=EulerMethod(Xinit, Xfin, Uinit, Tinit, Tfin, Nt, @KerMat_Diffusion_Isolated, D); toc
plot(X, Uinit, X, Ufixbc, '+', X, Uperiod, 'o', X, Uisolated, 'x')
legend('initial', 'fixed boundary', 'periodic', 'isolated')
