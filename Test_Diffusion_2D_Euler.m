% A script to test EulerMethod for 2D Diffusion

clear
close all

% Grid:
Xinit=0; Xfin=1; Nx=50; dx=(Xfin-Xinit)/(Nx-1);
Yinit=0; Yfin=1; Ny=50; dy=(Yfin-Yinit)/(Ny-1);
% Yinit=0; Yfin=1.5; Ny=75; dy=(Yfin-Yinit)/(Ny-1);
Tinit=0; Tfin=0.01; Nt=10000; dt=(Tfin-Tinit)/Nt;
x = Xinit:dx:Xfin;
y = Yinit:dy:Yfin;
[X, Y] = meshgrid(x, y);
Uinit = zeros(Ny, Nx);

% Test function 1: Plane wave
A=1;
k=[2.2 0.5];
phi=1;
for i = 1 : Ny
    for j = 1 : Nx
        Uinit(i,j) = A * cos(2 * pi * k * [x(j); y(i)] - phi);
    end
end

% Test function 2: Gaussian
% A=1;
% sigm=[0.3 0; 0 0.3];
% mu = [0.4 0.2];
% for i = 1 : Ny
%     for j = 1 : Nx
%         Uinit(i,j) = (A/det(sigm)/2/pi)*exp(-[x(j)-mu(1) y(i)-mu(2)]*inv(sigm)*[x(j)-mu(1); y(i)-mu(2)]/2);
%     end
% end

% Showing initial field and pause:
figure(1)
image([Xinit, Xfin], [Yinit, Yfin], Uinit, 'CDataMapping', 'scaled')
% surf(X, Y, Uinit)
colorbar; lim = caxis; title('Initial')
pause

D=1;  % Diffusion coefficient

% Solve and display for fixed BC:
disp('Fixed BC:')
tic; Ufixbc = EulerMethod([Yinit, Xinit], [Yfin, Xfin], Uinit, Tinit, Tfin, Nt, @KerMat_Diffusion_FixedBC, D); toc
figure(2); image([Xinit, Xfin], [Yinit, Yfin], Ufixbc, 'CDataMapping', 'scaled')
% caxis(lim);
colorbar; title('Fixed BC')

% Solve and display for periodic BC:
disp('Periodic:')
tic; Uperiod = EulerMethod([Yinit, Xinit], [Yfin, Xfin], Uinit, Tinit, Tfin, Nt, @KerMat_Diffusion_Periodic, D); toc
figure(3); image([Xinit, Xfin], [Yinit, Yfin], Uperiod, 'CDataMapping', 'scaled')
% caxis(lim);
colorbar; title('Periodic')

% Solve and display for isolated walls:
disp('Isolated:')
tic; Uisol = EulerMethod([Yinit, Xinit], [Yfin, Xfin], Uinit, Tinit, Tfin, Nt, @KerMat_Diffusion_Isolated, D); toc
figure(4); image([Xinit, Xfin], [Yinit, Yfin], Uisol, 'CDataMapping', 'scaled')
% caxis(lim);
colorbar; title('Isolated')
